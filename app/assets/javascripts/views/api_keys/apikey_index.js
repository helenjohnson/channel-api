RailsApi.Views.ApiKeyView = Backbone.View.extend({
  tagName: 'li',
  template: JST['api_keys/index'],

  initialize: function() {
    // this.listenTo(this.model, 'change', this.render);
    // this.listenTo(this.model, 'destroy', this.remove);    
  },

  render: function() { 
    var apiKey = this.template(this.model.attributes);
    this.$el.append(apiKey);
    return this;
  },

  events: {
    'click .delete-key': 'deleteKey'
  },

  deleteKey: function() {
    var that = this;
    openModal('.user', 'Are you sure you want to delete this key?');

    $('.user .modal .confirm').click(function() {
      closeModal();
      that.model.url = '/users/'+that.model.user_id+'/api_keys/'+that.model.id;
      that.model.destroy({
        success: function(model, response) {
          notify(response.msg, 'success'); }
      }, {
        error: function(model, response) {
          notify(response.msg, 'error'); }
      });
    });    
  }
});

RailsApi.Views.ApiKeysView = Backbone.View.extend({
  initialize: function() {
    this.listenTo(this.collection, 'all', this.render);
  },

  render: function() {
    var that = this;
    this.$el.empty();

    _.each(this.collection.models, function(api_key) {
      var apiKeyView = new RailsApi.Views.ApiKeyView({model: api_key
      });
      
      that.$el.append(apiKeyView.render().el);
    });
  }

});