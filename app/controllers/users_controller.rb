class UsersController < ApplicationController
  before_action :require_login, only: [:index, :show, :update]

  def index
    # Current user template
  end

  def create
    info = User.create_new(user_params)
    render json: { msg: info[:msg], status: info[:status] }
  end

  def show 
    render json: current_user.to_json  
  end

  def update
    User.update(user_params[:id], user_params)
    render json: { msg: 'Account updated.' } 
  end

  def destroy
  end

  private
  def user_params
    params.require(:user).permit(:email, :organization, :password, :new_password, :id, :crypted_password, :salt)
  end
end