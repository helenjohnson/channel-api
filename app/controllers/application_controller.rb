class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  helper_method :request_url
                #:waywire_user #Uncomment in production
  skip_before_filter :verify_authenticity_token

  def waywire_user # auto log in
    session_hash = cookies[:mvp_session]
    authentication = HTTParty.get(session_hash)
    user = authentication['user']['handle']
  end

  def http_request
    request.headers  #['HTTP_ORIGIN']
  end

end
