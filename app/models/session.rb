class Session < ActiveRecord::Base

  def self.create_new(user, params)
    if user == nil
      status  = 'error' 
      user_id = nil
      if User.find_by(email: params[:email]) == nil
        msg    = 'Email does not exist in our record.'
      else
        msg    = 'Password is incorrect.'
      end 
    else    
      status  = 'success'
      msg     = '' 
      user_id = user.id 
    end
    { msg: msg, status: status, user_id: user_id }
  end  

end