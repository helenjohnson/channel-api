require 'rails_helper'

RSpec.describe Channel, type: :model do 

  before :each do 
    @channel = Channel.create_or_update(
      href: 'http://dailydose.waywire.com', 
      name: 'daily dose', 
      category_array: ['curated','news']
    )
  end

  # Channel attributes
  it 'has unique href attribute' do 
    Channel.create_or_update(
      href: 'http:/dailydose.waywire.com', 
      name: 'Daily Dose', 
      category_array: ['curated','news']
    )
    expect(Channel.where(href: 'http://dailydose.waywire.com').length).to eq(1)
  end
  
  it 'does not create two channels with the same href' do
    Channel.create_or_update(
      href: 'http://dailydose.waywire.com', 
      name: 'Daily Dose', 
      category_array: ['curated','news']
    )
    expect(Channel.all.length).to eq(1)
  end

  it 'category_array attribute is an array' do 
    expect(@channel.category_array.class).to eq(Array)
  end

  # Channel create_or_update, update_with_id methods
  it 'updates channel if it already exists' do 
    Channel.create_or_update(
      href: 'http://dailydose.waywire.com', 
      name: 'Daily Dose', 
      category_array: ['curated','lifestyle']
    )
    @channel.reload
    expect(@channel.category_array).to eq(['curated','lifestyle'])    
  end

  it 'updates an existing channel' do 
    Channel.update_with_id(id: @channel.id, name: 'Daily Dose')
    @channel.reload
    expect(@channel.name).to eq('Daily Dose')
  end

  # Channel - category relation
  it 'gets added to the correct category' do
    curated = Category.create(name: 'curated')
    Channel.add_to_category(@channel)    
    @channel.categories.should include(curated) 
  end

  it 'gets added to the correct categories' do
    curated = Category.create(name: 'curated')
    news = Category.create(name: 'news')
    Channel.add_to_category(@channel)    
    @channel.categories.length.should eq(2) 
  end
end