require_relative './channels_info'
channels   = ChannelInfo.all
categories = ChannelInfo.categories

channels.each do |channel_params|
  channel_params[:category_array] = channel_params[:category_array].downcase.split(',')
  Channel.create_or_update(channel_params) 
end

categories.each do |name|
  Category.find_or_create_by(name: name)
end

Category.all.each do |category|
  Channel.all.each do |channel|
    if channel.category_array.include? category.name
      unless category.channels.include?(channel)
        category.channels << channel
      end  
    end
  end
end

user = User.create(email: 'devteam@waywire.com', password: 'WWdev135')
key = ApiKey.create(access_token: 'da388efde809cb227c3d43ab2f170e5a', user_id: user.id)
key.update(access_token: 'da388efde809cb227c3d43ab2f170e5a')

