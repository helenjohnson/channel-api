# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150108230614) do

  create_table "categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories_channels", force: true do |t|
    t.integer  "category_id"
    t.integer  "channel_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "channels", force: true do |t|
    t.string   "name"
    t.string   "src"
    t.string   "href"
    t.string   "category_array"
    t.string   "desc"
    t.string   "keywords"
    t.string   "curator_name"
    t.string   "curator_img"
    t.string   "curator_video"
    t.string   "curator_desc"
    t.string   "banner_img"
    t.string   "mobile_banner"
    t.string   "twitter_widget_id"
    t.integer  "community_site_nid"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "waywiremanager_api_keys", force: true do |t|
    t.string   "access_token"
    t.string   "role"
    t.integer  "user_id"
    t.date     "expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "waywiremanager_users", force: true do |t|
    t.string "email",            null: false
    t.string "crypted_password", null: false
    t.string "salt",             null: false
    t.string "organization"
  end

end
